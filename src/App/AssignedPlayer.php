<?php

namespace Hatem\App;

use Hatem\Contracts\PlayerContracts;
use Hatem\Contracts\ClubContracts;

 

class AssignedPlayer implements PlayerContracts,ClubContracts
{
    private $name;
    private $nationality;
    private $club;

    public function setName(string $string)
    {
        $this->name = $string;
    }

    public function getName()
    {
       return $this->name; 
    }
    
    public function setNationality(string $nation)
    {
        $this->nationality = $nation;
    }
    
    public function getNationality()
    {
        return $this->nationality;
    }
    public function setClub(string $string)
    {
        $this->club = $string;
    }
    
    public function getClub()
    {
        return $this->club;
    }
    

}