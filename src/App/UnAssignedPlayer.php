<?php

namespace Hatem\App;
use Hatem\contracts\PlayerContracts;

class UnAssignedPlayer implements PlayerContracts
{
    private $name;
    private $nationality;
    
    public function setName(string $string)
    {
        $this->name = $string;
    }

    public function getName()
    {
        return $this->name;
    }
    public function setNationality(string $nation)
    {
        $this->nationality = $nation;
    }
    
    public function getNationality()
    {
        return $this->nationality;
    }

}