<?php

namespace Hatem\App;

use Hatem\Contracts\PlayerContracts;

class Player
{

    private $player;

    /**
     * @param PlayerContracts $player
     * @param array $data
     */
    public function __construct(PlayerContracts $player, array $data)
    {
        $player->setName($data['name']);
        $player->setNationality($data['nationality']);
        if($player instanceof AssignedPlayer){
            $player->setClub($data['club']);
        }

        $this->player =$player;



    }

    public function getPlayer(){
        return $this->player;
    }

     

}