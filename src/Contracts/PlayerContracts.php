<?php

namespace Hatem\Contracts;

interface PlayerContracts
{
    public function setName(string $string);
    public function getName();
    public function setNationality(string $nation);
    public function getNationality();

}